#!/bin/sh

sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get update
sudo apt-get install mongodb-10gen
sudo mkdir -p /data/db
sudo chmod -R 777 /data/db
sudo chmod -R 777 /var/log/mongodb
sudo chmod -R 777 /var/lib/mongodb

#!append conf file

// restart services
sudo service mongodb restart

