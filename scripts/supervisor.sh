#!/bin/sh
echo "** INSTALLING SUPERVISOR **"
sudo apt-get install -y supervisor
sudo echo [program:queue] >> /etc/supervisor/conf.d/queue.conf
sudo echo command=php artisan queue:listen --tries=2 >> /etc/supervisor/conf.d/queue.conf
sudo echo directory=/home/vagrant/code/es-rebuild >> /etc/supervisor/conf.d/queue.conf
sudo echo stdout_logfile=/home/vagrant/code/es-rebuild/app/storage/logs/supervisor.log >> /etc/supervisor/conf.d/queue.conf
sudo echo redirect_stderr=true >> /etc/supervisor/conf.d/queue.conf

echo "** STARTING SUPERVISOR **"
sudo service supervisor restart