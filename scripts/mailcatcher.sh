#!/bin/sh
echo "** INSTALLING MAILCATCHER **"
sudo apt-get install ruby-dev libsqlite3-dev
sudo gem install mailcatcher
mailcatcher --http-ip 0.0.0.0

echo "** RESTARTING THINGS **"
sudo service nginx restart
sudo service php5-fpm restart