class Homestead
  def Homestead.configure(config, settings)
    # Configure The Box
    config.vm.box = "laravel/homestead"
    config.vm.hostname = "homestead"

    # Configure A Private Network IP
    config.vm.network :private_network, ip: settings["ip"] ||= "192.168.10.10"

    # Configure A Few VirtualBox Settings
    config.vm.provider "virtualbox" do |vb|
      vb.customize ["modifyvm", :id, "--memory", settings["memory"] ||= "2048"]
      vb.customize ["modifyvm", :id, "--cpus", settings["cpus"] ||= "1"]
      vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    # Config Github Settings
    github_username = "fideloper"
    github_repo     = "Vaprobash"
    github_branch   = "1.0.1"
    github_url      = "https://raw.githubusercontent.com/#{github_username}/#{github_repo}/#{github_branch}"



    # Configure Port Forwarding To The Box

    config.vm.network "forwarded_port", guest: 80, host: 8000

    # MySQL Port
    config.vm.network "forwarded_port", guest: 3306, host: 33060

    # PostgreSQL Port
    config.vm.network "forwarded_port", guest: 5432, host: 54320

    # MongoDB Port
    # config.vm.network "forwarded_port", guest: 27017, host: 27071

    # MailCather Port
    config.vm.network "forwarded_port", guest: 1080, host: 10800

    # Configure The Public Key For SSH Access
    config.vm.provision "shell" do |s|
      s.inline = "echo $1 | tee -a /home/vagrant/.ssh/authorized_keys"
      s.args = [File.read(File.expand_path(settings["authorize"]))]
    end

    # Copy The SSH Private Keys To The Box
    settings["keys"].each do |key|
      config.vm.provision "shell" do |s|
        s.privileged = false
        s.inline = "echo \"$1\" > /home/vagrant/.ssh/$2 && chmod 600 /home/vagrant/.ssh/$2"
        s.args = [File.read(File.expand_path(key)), key.split('/').last]
      end
    end

    # Copy The Bash Aliases
    config.vm.provision "shell" do |s|
      s.inline = "cp /vagrant/aliases /home/vagrant/.bash_aliases"
    end

    # Register All Of The Configured Shared Folders
    settings["folders"].each do |folder|
      config.vm.synced_folder folder["map"], folder["to"], type: folder["type"] ||= nil
    end

    # Install All The Configured Nginx Sites
    settings["sites"].each do |site|
      config.vm.provision "shell" do |s|
          s.inline = "bash /vagrant/scripts/serve.sh $1 $2"
          s.args = [site["map"], site["to"]]
      end
    end

    # Configure All Of The Server Environment Variables
    if settings.has_key?("variables")
      settings["variables"].each do |var|
        config.vm.provision "shell" do |s|
            s.inline = "echo \"\nenv[$1] = '$2'\" >> /etc/php5/fpm/php-fpm.conf && service php5-fpm restart"
            s.args = [var["key"], var["value"]]
        end
      end
    end

    # not quite working yet, needs some more work - TBD
    # Provision MongoDB
    # config.vm.provision "shell", path: "scripts/mongodb.sh"


    # Provision MailCatcher
    config.vm.provision "shell", path: "scripts/mailcatcher.sh"

    # Provision Supervisor
    # NOT WORKING YET!
    # config.vm.provision "shell", path: "scripts/supervisor.sh"

  end
end
